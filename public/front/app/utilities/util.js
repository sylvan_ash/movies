/**
 * Represents the utils
 */

(function() {
    'use strict'

    angular.module('app')
        .factory('util', function($log) { // $uibModal
            var util = {

                /**
                 * Handle error from API
                 * @param {Object} response the api response
                 */
                handleError: function(response) {
                    $log.error(response)
                    if (response.data) {
                        util.showError(response.data.error || 'An error occurred.')
                    } else {
                        util.showError('Cannot connect to server. Please check your internet access.')
                    }
                },

                /**
                 * Show error message
                 * @param {String} msg the message
                 */
                showError: function(msg) {
                    util.showAlert('Error', msg)
                },

                /**
                 * Show alert message
                 * @param {String} title the alert title
                 * @param {String} msg the message
                 * @return {Object} the modal instance
                 */
                showAlert: function(title, msg) {
                    // return $uibModal.open({
                    //     templateUrl: 'app/common/popups/alert.html',
                    //     controller: function($scope) {
                    //         $scope.title = title
                    //         $scope.msg = msg
                    //     },
                    //     size: 'md'
                    // })
                    console.log(`${title}: ${msg}`)
                    return {}
                },

                /**
                 * Show confirm dialog
                 * @param {String} title the alert title
                 * @param {String} msg the message
                 * @return {Object} the modal instance
                 */
                showConfirm: function(title, msg) {
                    // return $uibModal.open({
                    //     templateUrl: 'app/common/popups/confirm.html',
                    //     controller: function($scope) {
                    //         $scope.title = title
                    //         $scope.msg = msg
                    //     },
                    //     size: 'md'
                    // })
                    console.log(`${title}: ${msg}`)
                    return {}
                }
            }
            return util
        })
})()
/* eslint-disable semi */
;
