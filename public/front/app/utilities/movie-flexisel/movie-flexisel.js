/* global $ */
/**
 * File upload directive
 */
(function() {
    'use strict'

    angular.module('app')
        .directive('mvFlexisel', function() {
            return {
                restrict: 'E',
                transclude: true,
                scope: {
                    options: '=',
                    items: '='
                },
                templateUrl: 'front/app/utilities/movie-flexisel/flex.html',
                link: function(scope, elem, attrs) {
                    console.info('flex')
                    console.log(scope.items.length)
                    var defaults = {
                        visibleItems: 6,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: false,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint: 480,
                                visibleItems: 2
                            },
                            landscape: {
                                changePoint: 640,
                                visibleItems: 3
                            },
                            tablet: {
                                changePoint: 768,
                                visibleItems: 4
                            }
                        }
                    }
                    defaults = _.extend(defaults, scope.options)
                    setTimeout(function() {
                        $("#flexiselDemo1").flexisel(defaults)
                    }, 1000)
                }
            }
        })
})()
/* eslint-disable semi */
;
