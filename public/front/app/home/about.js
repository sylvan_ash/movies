/**
 * Represents the home page
 */

(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            var base = 'front/app/home'
            $stateProvider
                .state('about', {
                    url: '^/about',
                    templateUrl: base + '/about.html',
                    controller: 'AboutController as vm'
                })
        })
        .controller('AboutController', function($scope) {
            var vm = this
            console.log('about')
        })
})()
/* eslint-disable semi */
;
