/**
 * Represents the home page
 */

(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            var base = 'front/app/home'
            $stateProvider
                .state('home', {
                    url: '^/',
                    templateUrl: base + '/home.html',
                    controller: 'HomeController as vm',
                    resolve: {
                        movies: function(Movie) {
                            return Movie.query({highlight: true, limited: true, limitType: 3}).$promise
                        }
                    }
                })
        })
        .controller('HomeController', function(movies) {
            var vm = this

            // Get main highlight movie
            var main
            while (_.isEmpty(main)) {
                var index = _.random(0, movies.length)
                main = movies.splice(index, 1)
            }

            vm.main = main[0]
            vm.highlights = movies
        })
})()
/* eslint-disable semi */
;
