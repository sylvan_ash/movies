/**
 * Represents main application module
 */

(function() {
    'use strict'

    angular.module('app', ['ui.router', 'ngResource'])
        .constant('config', window.config)
        // .factory('currentUser', function() {
        //     var user = {
        //         /**
        //          * Set user data
        //          * @param {Object} data the user data
        //          */
        //         login: function(data) {
        //             user.authenticated = Boolean(data.id)
        //             user.id = data.id
        //             user.username = data.username
        //             user.sa = data.role === 'Super Admin'
        //             user.admin = user.sa || data.role === 'Client Admin'
        //             user.agreedToTerms = data.agreedToTerms || false
        //             user.group = data.group || null
        //         }
        //     }
        //     user.login(window.user || {})
        //     return user
        // })
        .config(['$locationProvider', function($locationProvider){
            $locationProvider.html5Mode({}).hashPrefix('');
        }])
        .config(function($urlRouterProvider, $logProvider, config) {
            $urlRouterProvider.otherwise('/')
            $logProvider.debugEnabled(config.DEBUG)
        })
        .config(function($httpProvider) {
            // fix caching for GET requests
            $httpProvider.interceptors.push(function() {
                return {
                    request: function(request) {
                        if (/^\/api/.test(request.url) && request.method === 'GET') {
                            request.headers['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT'
                        }
                        return request
                    }
                }
            })
        })
        .run(function($rootScope, $log, $state) {
            // $rootScope.currentUser = currentUser

            // // logout user and refresh page
            // $rootScope.logout = function() {
            //     User.logout(function() {
            //         window.location.reload()
            //     }, util.handleError)
            // }

            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                $log.error('$stateChangeError', toState, error)
            })

            // $rootScope.$on('$stateChangeStart', function(event, toState) {
            //     if (!toState || toState.public) {
            //         return
            //     }
            //     if (!currentUser.authenticated) {
            //         event.preventDefault();
            //         $state.go("login");
            //         return;
            //     }
            // })
        })
})()
/* eslint-disable semi */
;
