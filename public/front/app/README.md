# NOTE
For the JS files, terminate the self-calling function with a semi-colon. This is so as the file concatenation will work as intended.
If a semi-colon is missing, a `(intermediate value)(intermediate value)(...) is not a function` error will be thrown on the browser
and the angular side of the app won't be instantiated

