(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            $stateProvider
                .state('users', {
                    url: '^/users',
                    templateUrl: 'app/users/users.html',
                    controller: 'UsersController as vm',
                    resolve: {
                        users: function(User) {
                            return User.query().$promise
                        }
                    }
                })
                .state('addUser', {
                    url: '^/users/add',
                    templateUrl: 'app/users/user.html',
                    controller: 'UserController as vm',
                    resolve: {
                        user: function() {
                            return null
                        }
                    }
                })
                .state('editUser', {
                    url: '^/users/:id',
                    templateUrl: 'app/users/user.html',
                    controller: 'UserController as vm',
                    resolve: {
                        user: function(User, $stateParams) {
                            return User.get({
                                id: $stateParams.id
                            })
                        }
                    }
                })
        })
        .controller('UsersController', function(users) {
            var vm = this
            vm.users = users
            vm.remove = remove

            /**
             * @param {Object} user User to delete
             */
            function remove(user) {
                user.$delete(function() {
                    users.splice(users.indexOf(user), 1)
                })
            }
        })
        .controller('UserController', function(user, User, $state) {
            var vm = this
            vm.user = user
            vm.save = save
            vm.cancel = cancel
            vm.isNew = !user

            /**
             * @param {Object} form User info to save
             */
            function save(form) {
                vm.validate = true
                if (form.$invalid || !vm.user || (vm.user.password && vm.user.password !== vm.user.confirmPassword)) {
                    return
                }
                User.save(vm.user, function() {
                    $state.go('users')
                })
            }

            /**
             * Cancel editing and go back to list
             */
            function cancel() {
                $state.go('users')
            }
        })
})()
/* eslint-disable semi */
;
