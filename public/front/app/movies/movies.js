/**
 * Represents the home page
 */

(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            var base = 'front/app/movies'
            $stateProvider
                .state('movies', {
                    url: '^/movies',
                    templateUrl: base + '/movies.html',
                    controller: 'MoviesController as vm'
                })
                .state('movieInfo', {
                    url: '^/movie/:id',
                    templateUrl: base + '/movie.html',
                    controller: 'MovieController as vm'
                })
        })
        .controller('MoviesController', function($scope) {
            var vm = this
            console.log('hey')
        })
        .controller('MovieController', function($scope) {
            var vm = this
            console.log('hey')
        })
})()
/* eslint-disable semi */
;
