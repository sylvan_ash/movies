/**
 * Represents the home page
 */

(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            var base = 'app/movies'
            $stateProvider
                .state('movies', {
                    url: '^/movies',
                    templateUrl: base + '/movies.html',
                    controller: 'MoviesController as vm',
                    resolve: {
                        movies: function(Movie) {
                            return Movie.query({limited: true, limitType: 1}).$promise
                        }
                    }
                })
                .state('addMovie', {
                    url: '^/movies/add',
                    templateUrl: base + '/movie.html',
                    controller: 'MovieController as vm',
                    resolve: {
                        movie: function() {
                            return {}
                        },
                        genres: function(Genre) {
                            return Genre.query().$promise
                        }
                    }
                })
                .state('editMovie', {
                    url: '^/movies/:id',
                    templateUrl: base + '/movie.html',
                    controller: 'MovieController as vm',
                    resolve: {
                        movie: function(Movie, $stateParams) {
                            return Movie.get({
                                id: $stateParams.id
                            })
                        },
                        genres: function(Genre) {
                            return Genre.query().$promise
                        }
                    }
                })
        })
        .controller('MoviesController', function(movies) {
            var vm = this
            vm.movies = movies
            vm.remove = remove

            /**
             * @param {Object} movie Movie to delete
             */
            function remove(movie) {
                movie.$delete(function() {
                    movies.splice(movies.indexOf(movie), 1)
                })
            }
        })
        .controller('MovieController', function(movie, genres, Movie, $state, util, Upload) {
            var vm = this
            var url = _.isEmpty(movie) ? '/api/movies' : `/api/movies/${movie.id}`
            vm.movie = movie
            vm.genres = genres
            vm.isNew = _.isEmpty(movie)
            vm.mpRatings = util.MPAA
            vm.genresHasValue = false
            vm.genreIds = []
            vm.search = {}
            vm.actor = {}
            vm.image = null
            vm.editImage = _.isEmpty(movie) ? null : movie.image
            vm.editHighlight = _.isEmpty(movie) ? null : movie.highlightImage
            vm.highlightImage = null
            vm.editDate = (!_.isEmpty(movie) && movie.releaseDate !== null) ? movie.releaseDate : null

            vm.save = save
            vm.cancel = cancel
            vm.toggleGenre = toggleGenre
            vm.isGenreSelected = isGenreSelected
            vm.addStar = addStar
            vm.addActor = addActor

            /**
             * @param {Object} form Movie info to save
             */
            function save(form) {
                vm.validate = true
                if (form.$invalid || !vm.movie) {
                    return
                }
                // highlights check

                var data = vm.movie
                if (vm.image) {
                    console.info('image')
                    data = _.extend(data, {uploadImage: vm.image})
                }
                if (vm.highlightImage) {
                    console.info('highlight')
                    data = _.extend(data, {uploadHighlight: vm.highlightImage})
                }
                console.log(data)

                // Remove $promise since we aren't working with Movie resource
                delete data.$promise

                Upload.upload({
                    url,
                    data: data
                }).then(function(resp) {
                    console.log(resp)
                    $state.go('movies')
                }, function(error) {
                    console.log(error)
                })

                // Movie.save(vm.movie, function() {
                //     $state.go('movies')
                // })
            }

            /**
             * Cancel editing and go back to list
             */
            function cancel() {
                $state.go('movies')
            }

            /**
             * Check if a genre has been selected or not
             * @param {Object} genre genre
             * @return {Boolean} true if genre has been selected
             */
            function isGenreSelected(genre) {
                if (!vm.movie.hasOwnProperty('genres')) {
                    return false
                }

                var selected = _.find(vm.movie.genres, function(item) {
                    return item.id === genre.id
                })

                if (selected) {
                    return true
                }

                return false
            }

            /**
             * Add/Remove a genre
             * @param {Object} genre Genre to be added / removed
             */
            function toggleGenre(genre) {
                // Make sure 'genres' exists
                if (!vm.movie.hasOwnProperty('genres')) {
                    vm.movie.genres = []
                }

                // Check if the genre has been added or not
                var selected = _.find(vm.movie.genres, function(item) {
                    return item.id === genre.id
                })

                // If added, remove it
                if (selected) {
                    var idx = vm.movie.genres.indexOf(selected)
                    vm.movie.genres.splice(idx, 1)

                // Else add it
                } else {
                    vm.movie.genres.push({id: genre.id, title: genre.title})
                }

                // Check if a genre has been selected
                if (vm.movie.genres.length > 0) {
                    vm.genresHasValue = true
                } else {
                    vm.genresHasValue = false
                }
            }

            /**
             * Adds a star to the movie
             */
            function addStar() {
                if (!vm.star || !vm.star.hasOwnProperty('originalObject') || !vm.star.title) {
                    return
                }

                if (!vm.movie.hasOwnProperty('stars')) {
                    vm.movie.stars = []
                }

                vm.movie.stars.push(vm.star.originalObject)
                vm.star = null
            }

            /**
             * Add an actor
             */
            function addActor() {
                if (!vm.actor || !vm.actor.name.hasOwnProperty('originalObject') || !vm.actor.role) {
                    return
                }

                if (!vm.movie.hasOwnProperty('actors')) {
                    vm.movie.actors = []
                }

                var actor = {
                    role: vm.actor.role,
                    actor: vm.actor.name.originalObject
                }
                vm.movie.actors.push(actor)
                vm.actor = {}
            }
        })
})()
/* eslint-disable semi */
;
