/**
 * Represents the home page
 */

(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            var base = 'app/dashboard'
            $stateProvider
                .state('dashboard', {
                    url: '^/',
                    templateUrl: base + '/dashboard.html',
                    controller: 'DashboardController as vm'
                })
        })
        .controller('DashboardController', function() {
            var vm = this
        })
})()
/* eslint-disable semi */
;
