/**
 * Represents the API resources
 */
(function() {
    'use strict'

    angular.module('app')
        .factory('User', function($resource) {
            return $resource('/api/users/:id', {id: '@id'}, {
                login: {
                    method: "POST",
                    url: '/api/login'
                },
                logout: {
                    method: "POST",
                    url: '/api/logout'
                }
            })
        })
        .factory('Movie', function($resource) {
            return $resource('/api/movies/:id', {id: '@id'})
        })
        .factory('Person', function($resource) {
            return $resource('/api/persons/:id', {id: '@id'})
        })
        .factory('Genre', function($resource) {
            return $resource('/api/genres/:id', {id: '@id'})
        })
})()
/* eslint-disable semi */
;
