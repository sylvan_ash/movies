/**
 * File upload directive
 */
(function() {
    'use strict'

    angular.module('app')
        .directive('mvFileModel', ['$parse', function($parse) {
            return {
                restrict: 'A',
                scope: {
                    fileModel: '&'
                },
                link: function(scope, element, attrs) {
                    element.bind('change', function() {
                        scope.$apply(function() {
                            console.log(scope)
                            // scope.vm.image = element[0].files[0]
                            scope.fileModel = element[0].files[0]
                        })
                    })
                }

            }
        }])
})()
/* eslint-disable semi */
;
