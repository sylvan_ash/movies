/**
 * Represents the home page
 */

(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            var base = 'app/persons'
            $stateProvider
                .state('persons', {
                    url: '^/persons',
                    templateUrl: base + '/persons.html',
                    controller: 'PersonsController as vm',
                    resolve: {
                        persons: function(Person) {
                            return Person.query({limited: true, limitType: 1}).$promise
                        }
                    }
                })
                .state('addPerson', {
                    url: '^/persons/add',
                    templateUrl: base + '/person.html',
                    controller: 'PersonController as vm',
                    resolve: {
                        person: function() {
                            return {}
                        }
                    }
                })
                .state('editPerson', {
                    url: '^/persons/:id',
                    templateUrl: base + '/person.html',
                    controller: 'PersonController as vm',
                    resolve: {
                        person: function(Person, $stateParams) {
                            return Person.get({
                                id: $stateParams.id
                            })
                        }
                    }
                })
        })
        .controller('PersonsController', function(persons) {
            var vm = this
            vm.persons = persons
            vm.remove = remove

            /**
             * @param {Object} person Person to delete
             */
            function remove(person) {
                person.$delete(function() {
                    persons.splice(persons.indexOf(person), 1)
                })
            }
        })
        .controller('PersonController', function(person, Person, $state) {
            var vm = this
            vm.person = person
            vm.save = save
            vm.cancel = cancel
            vm.isNew = !person

            /**
             * @param {Object} form Person info to save
             */
            function save(form) {
                vm.validate = true
                if (form.$invalid || _.isEmpty(vm.person) || !vm.person.name || !vm.person.slug) {
                    return
                }
                Person.save(vm.person, function() {
                    $state.go('persons')
                })
            }

            /**
             * Cancel editing and go back to list
             */
            function cancel() {
                $state.go('persons')
            }
        })
})()
/* eslint-disable semi */
;
