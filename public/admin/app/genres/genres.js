/**
 * Represents the home page
 */

(function() {
    'use strict'

    angular.module('app')
        .config(function($stateProvider) {
            var base = 'app/genres'
            $stateProvider
                .state('genres', {
                    url: '^/genres',
                    templateUrl: base + '/genres.html',
                    controller: 'GenresController as vm',
                    resolve: {
                        genres: function(Genre) {
                            return Genre.query().$promise
                        }
                    }
                })
                .state('addGenre', {
                    url: '^/genres/add',
                    templateUrl: base + '/genre.html',
                    controller: 'GenreController as vm',
                    resolve: {
                        genre: function() {
                            return null
                        }
                    }
                })
                .state('editGenre', {
                    url: '^/genres/:id',
                    templateUrl: base + '/genre.html',
                    controller: 'GenreController as vm',
                    resolve: {
                        genre: function(Genre, $stateParams) {
                            return Genre.get({
                                id: $stateParams.id
                            })
                        }
                    }
                })
        })
        .controller('GenresController', function(genres) {
            var vm = this
            vm.genres = genres
            vm.remove = remove

            /**
             * @param {Object} genre Genre to delete
             */
            function remove(genre) {
                genre.$delete(function() {
                    genres.splice(genres.indexOf(genre), 1)
                })
            }
        })
        .controller('GenreController', function(genre, Genre, $state) {
            var vm = this
            vm.genre = genre
            vm.save = save
            vm.cancel = cancel
            vm.isNew = !genre

            /**
             * @param {Object} form Genre info to save
             */
            function save(form) {
                vm.validate = true
                if (form.$invalid || !vm.genre || !vm.genre.title) {
                    return
                }
                Genre.save(vm.genre, function() {
                    $state.go('genres')
                })
            }

            /**
             * Cancel editing and go back to list
             */
            function cancel() {
                $state.go('genres')
            }
        })
})()
/* eslint-disable semi */
;
