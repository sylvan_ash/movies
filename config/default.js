module.exports = {
    WEB_SERVER_PORT: 3300,
    MONGODB_URL: 'mongodb://127.0.0.1:27017/movies',
    LOG_LEVEL: 'debug',
    SECURITY: {
        SALT_LENGTH: 64,
        ITERATIONS: 4096,
        PASSWORD_LENGTH: 64
    },
    SESSION_SECRET: 'top0der',
    STORAGE_PATH: '/tmp',
    AWS_ACCESS_KEY: 'update',
    AWS_SECRET_KEY: 'update',
    S3_BUCKET: 'update',
    AWS_REGION: 'update',
    UPLOADS: '../public/uploads',
    HIGHLIGHTS_LIMIT: 7,

    PUBLIC: {
        DEBUG: true
    }
}
