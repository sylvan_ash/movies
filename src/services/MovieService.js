'use strict'

var _ = require('underscore')
var models = require('../models')
var Movie = models.Movie
var NotFoundError = require('../utilities/errors').NotFoundError
var ValidationError = require('../utilities/errors').ValidationError
var logger = require('../utilities/logger')
var util = require('../utilities/utils')
var LimitTypes = require('../utilities/const').LimitTypes
var fs = require('fs')
var config = require('config')

/**
 * Create a new movie
 * @param {Object} data values to save
 * @param {Array|null} files image files for the movie
 * @return {Object} new movie
 * @throws {ValidationError} if url is empty
 */
function * createMovie(data, files) {
    var values = _.pick(data, ['title', 'description', 'slug', 'releaseDate', 'mpRating', 'rating', 'actors', 'stars', 'directors', 'highlight', 'genres', 'image'])
    if (!values.hasOwnProperty('slug') || !values.slug) {
        throw new ValidationError(`A unique slug is required!`)
    }

    if (!_.isEmpty(files)) {
        // console.log(files)
        _.each(files, function(item, key) {
            console.log(`File: ${key}`)
            if (['uploadImage', 'uploadHighlight'].indexOf(key) > -1) {
                var _key = key === 'uploadImage' ? 'image' : 'highlightImage'
                var uuid = item.uuid
                var field = item.field
                var filename = item.filename

                // Remove old file [Update]
                // if (values.hasOwnProperty(key) && values[_key] !== null && values[key] !== loc) {
                //     fs.unlink(values[key], function(error) {
                //         console.info('FILE Removal')
                //         console.log(error)
                //     })
                // }

                // Add new
                values[_key] = `/uploads/${uuid}/${field}/${filename}`
            }
        })
    }

    var existing = yield Movie.findOne({slug: values.slug})
    if (existing) {
        var unique = util.getRandomInt() + '' + util.getRandomInt()
        values.slug += `-${unique}`
    }

    if (values.hasOwnProperty('releaseDate')) {
        values.releaseDate = new Date(values.releaseDate)
    }

    return yield Movie.create(values)
}

/**
 * Update movie
 * @param {String} id ID of movie to update
 * @param {Object} data data to update with
 * @param {Array|null} files image files for the movie
 * @return {Object} the updated movie
 * @throws {NotFoundError} if movie is not found
 */
function * updateMovie(id, data, files) {
    var values = _.pick(data, ['title', 'description', 'slug', 'releaseDate', 'mpRating', 'rating', 'actors', 'stars', 'directors', 'highlight', 'genres', 'image', 'highlightImage'])

    var movie = yield getMovieExisting(id)

    // Check if the slug was changed to be a `slug` for another movie
    var existing = yield Movie.findOne({slug: values.slug, _id: {$ne: id}})
    if (existing) {
        var unique = util.getRandomInt() + '' + util.getRandomInt()
        values.slug += `-${unique}`
    }

    if (!_.isEmpty(files)) {
        // console.log(files)
        _.each(files, function(item, key) {
            // console.log(`File: ${key}`)
            if (['uploadImage', 'uploadHighlight'].indexOf(key) > -1) {
                var _key = key === 'uploadImage' ? 'image' : 'highlightImage'
                var uuid = item.uuid
                var field = item.field
                var filename = item.filename
                var loc = `/uploads/${uuid}/${field}/${filename}`

                // Remove old file [Update]
                if (values.hasOwnProperty(_key) && values[_key] !== null && values[_key] !== loc) {
                    var idx = item.file.indexOf('/uploads')
                    var fileLocation = item.file.substring(0, idx)
                    fileLocation += `${values[_key]}`

                    fs.unlink(fileLocation, function(error) {
                        console.info('FILE Removal')
                        console.log(error)
                    })
                }

                // Add new
                values[_key] = loc
            }
        })
    }

    _.extend(movie, values)
    yield movie.save()
    return movie
}

/**
 * Get movie
 * @param {Object} criteria Criteria to filter results
 * @param {String|null} limitType How to limit the results returned
 * @param {String|null} sort Field to use to sort the results
 * @return {Array} an array of movies
 */
function * getMovies(criteria, limitType, sort) {
    var query
    if (limitType) {
        var fields = ''
        switch (limitType) {
        case LimitTypes.BASIC:
            query = Movie.find(criteria || {}, 'title slug id image')
            break
        case LimitTypes.HIGHLIGHTS:
            fields = 'title description slug releaseDate mpRating rating stars genres image highlightImage'
            query = Movie.find(criteria || {}, fields).populate('stars').limit(config.HIGHLIGHTS_LIMIT)
            break
        default:
            query = Movie.find(criteria || {}, 'title slug id image')
            break
        }
    } else {
        query = Movie.find(criteria || {})
    }

    return yield query.sort(sort || 'title')
}

/**
 * Get movie
 * @param {String} id ID of movie to find
 * @param {Object} criteria criteria to filter by
 * @return {Object} the movie
 */
function * getMovie(id, criteria) {
    var movie = yield Movie.findById(id)
    return movie
}

/**
 * Get movie that exists
 * @param {String} id ID of movie to find
 * @throws {NotFoundError} if movie is not found
 * @return {Object} the movie
 */
function * getMovieExisting(id) {
    var movie = yield Movie.findById(id)
    if (!movie) {
        throw new NotFoundError(`Movie with id ${id} does not exist`)
    }

    return movie
}

/**
 * Delete movie
 * @param {String} id ID of movie to delete
 * @throws {NotFoundError} if movie is not found
 */
function * deleteMovie(id) {
    var movie = yield getMovieExisting(id)
    yield movie.remove()
}

module.exports = {
    createMovie,
    getMovies,
    getMovie,
    updateMovie,
    deleteMovie
}

logger.wrapFunctions(module.exports)
