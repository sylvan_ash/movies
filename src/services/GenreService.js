'use strict'

var _ = require('underscore')
var models = require('../models')
var Genre = models.Genre
var errors = require('../utilities/errors')
var NotFoundError = errors.NotFoundError
var ValidationError = errors.ValidationError
var logger = require('../utilities/logger')

/**
 * Create a new genre
 * @param {Object} data info to save
 * @return {Object} newly created genre
 * @throws {ValidationError} if title is empty
 */
function * createGenre(data) {
    var values = _.pick(data, ['title'])
    if (!values.hasOwnProperty('title') || !values.title) {
        throw new ValidationError(`A title is required to create a genre`)
    }

    return yield Genre.create(values)
}

/**
 * Get genre
 * @param {Object} criteria search criteria
 * @return {Array} array of genres
 */
function * getGenres(criteria) {
    return yield Genre.find(criteria || {}).sort('title')
}

/**
 * Get genre
 * @param {String} id genre id
 * @return {Object} genre
 */
function * getGenre(id) {
    var genre = yield Genre.findById(id)
    return genre
}

/**
 * Get genre that exists
 * @param {String} id genre id
 * @return {Object} genre
 * @throws {NotFoundError} if genre is not found
 */
function * getGenreExisting(id) {
    var genre = yield getGenre(id)
    if (!genre) {
        throw new NotFoundError(`Genre with id ${id} does not exist`)
    }

    return genre
}

/**
 * Update genre
 * @param {String} id genre id
 * @param {Object} values new genre values
 * @return {Object} updated genre
 * @throws {NotFoundError} if genre is not found
 */
function * updateGenre(id, values) {
    var genre = yield getGenreExisting(id)

    _.extend(genre, values)
    yield genre.save()
    return genre
}

/**
 * Delete genre
 * @param {String} id genre id
 * @throws {NotFoundError} if genre is not found
 */
function * deleteGenre(id) {
    var genre = yield getGenreExisting(id)
    yield genre.remove()
}

module.exports = {
    createGenre,
    getGenres,
    getGenre,
    updateGenre,
    deleteGenre
}

logger.wrapFunctions(module.exports)
