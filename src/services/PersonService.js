'use strict'

var _ = require('underscore')
var models = require('../models')
var Person = models.Person
var errors = require('../utilities/errors')
var NotFoundError = errors.NotFoundError
var ValidationError = errors.ValidationError
var logger = require('../utilities/logger')
var util = require('../utilities/utils')
var LimitTypes = require('../utilities/const').LimitTypes

/**
 * Create a new person
 * @param {Object} data info to save
 * @return {Object} created person
 * @throws {ValidationError} if url is empty
 */
function * createPerson(data) {
    var values = _.pick(data, ['name', 'dob', 'bio', 'slug', 'movies', 'roles'])
    if (!values.hasOwnProperty('slug') || !values.slug) {
        throw new ValidationError(`A unique SLUG is required!`)
    }

    var existing = yield Person.findOne({slug: values.slug})
    if (existing) {
        var unique = util.getRandomInt() + '' + util.getRandomInt()
        values.slug += `-${unique}`
    }

    return yield Person.create(values)
}

/**
 * Get person
 * @param {Object} criteria search criteria
 * @param {Number} limitType how to limit response data
 * @param {String} sort field to use to sort data with
 * @return {Array} an array of persons
 */
function * getPersons(criteria, limitType = null, sort = 'name') {
    var query
    console.log(limitType)

    if (limitType) {
        limitType = Number(limitType)
        switch (limitType) {
        case LimitTypes.MINIMAL:
            query = Person.find(criteria || {}, 'name id')
            break
        case LimitTypes.BASIC:
            query = Person.find(criteria || {}, 'name slug image')
            break
        default:
            query = Person.find(criteria || {})
            break
        }
    } else {
        query = Person.find(criteria || {})
    }

    return yield query.sort(sort)
}

/**
 * Get person
 * @param {String} id id of person
 * @param {Object} criteria some criteria
 * @return {Object} found person
 */
function * getPerson(id, criteria) {
    var person = yield Person.findById(id)
    if (!person) {
        person = yield Person.findOne({url: id})
    }

    return person
}

/**
 * Get person that exists
 * @param {String} id id of person
 * @return {Object} found person
 * @throws {NotFoundError} if person is not found
 */
function * getPersonExisting(id) {
    var person = yield getPerson(id)
    if (!person) {
        throw new NotFoundError(`Person with id/url ${id} does not exist`)
    }

    return person
}

/**
 * Update person
 * @param {String} id id of person
 * @param {Object} values values to update with
 * @return {Object} updated person
 * @throws {NotFoundError} if person is not found
 */
function * updatePerson(id, values) {
    var person = yield getPersonExisting(id)

    var existing = yield Person.findOne({url: values.url, _id: {$ne: id}})
    if (existing) {
        var unique = util.getRandomInt() + '' + util.getRandomInt()
        values.url += `-${unique}`
    }

    _.extend(person, values)
    yield person.save()
    return person
}

/**
 * Delete person
 * @param {String} id id of person
 * @throws {NotFoundError} if person is not found
 */
function * deletePerson(id) {
    var person = yield getPersonExisting(id)
    yield person.remove()
}

module.exports = {
    createPerson,
    getPersons,
    getPerson,
    updatePerson,
    deletePerson
}

logger.wrapFunctions(module.exports)
