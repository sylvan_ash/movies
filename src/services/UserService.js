'use strict'

var _ = require('underscore')
var models = require('../models')
var NotFoundError = require('../utilities/errors').NotFoundError
var ValidationError = require('../utilities/errors').ValidationError
var User = models.User
var logger = require("../utilities/logger")

/**
 * Create a new user
 * @param {Object} values the user values
 * @return {User} the created user instance
 * @throws {ValidationError} if username is taken or email is taken
 */
function * createUser(values) {
    var existing = yield User.findOne({username: values.username})
    if (existing) {
        throw new ValidationError(`Username ${values.username} is already taken`)
    }

    existing = yield User.findOne({email: values.email})
    if (existing) {
        throw new ValidationError(`Email ${values.email} is already taken`)
    }

    // NOTE: Salt password

    return yield User.create(values)
}

/**
 * Get users
 * @param {Object} criteria the search criteria
 * @return {User[]} the users
 */
function * getUsers(criteria) {
    // return yield User.find(criteria || {}).populate('group')
    return yield User.find(criteria || {}).sort('name')
}

/**
 * Get user by id
 * @param {String|ObjectId} id the user id
 * @return {User} the user
 * @throws {NotFoundError} if user is not found
 */
function * getUser(id) {
    var user = yield User.findById(id)
    if (!user) {
        throw new NotFoundError(`User with id ${id} does not exist`)
    }

    return user
}

/**
 * Delete user
 * @param {String|ObjectId} id the user id
 * @throws {NotFoundError} if user is not found
 */
function * deleteUser(id) {
    var user = yield getUser(id)
    yield user.remove()
}

/**
 * Update user
 * @param {String|ObjectId} id the user id
 * @param {Object} values the values to update
 * @return {Object} User
 * @throws {NotFoundError} if user is not found
 */
function * updateUser(id, values) {
    var user = yield getUser(id)

    var existing = yield User.findOne({username: values.username, _id: {$ne: id}})
    if (existing) {
        throw new ValidationError(`Username ${values.username} is already taken`)
    }
    existing = yield User.findOne({email: values.email, _id: {$ne: id}})
    if (existing) {
        throw new ValidationError(`Email ${values.email} is already taken`)
    }

    _.extend(user, values)
    yield user.save()
    return user
}

module.exports = {
    createUser: createUser,
    getUsers: getUsers,
    deleteUser: deleteUser,
    updateUser: updateUser,
    getUser: getUser
}

logger.wrapFunctions(module.exports)
