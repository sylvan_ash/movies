module.exports = {
    '/users': {
        get: {
            ctrl: 'UserController',
            method: 'getUsers',
            role: 'admin',
            public: true
        },
        post: {
            ctrl: 'UserController',
            method: 'createUser',
            role: 'admin',
            public: true
        }
    },
    '/users/:id': {
        get: {
            ctrl: 'UserController',
            method: 'getUser',
            role: 'admin'
        },
        post: {
            ctrl: 'UserController',
            method: 'updateUser',
            role: 'admin'
        },
        delete: {
            ctrl: 'UserController',
            method: 'deleteUser',
            role: 'admin'
        }
    },
    '/movies': {
        get: {
            ctrl: 'MovieController',
            method: 'getMovies',
            public: true
        },
        post: {
            ctrl: 'MovieController',
            method: 'createMovie',
            role: 'admin',
            public: true
        }
    },
    '/movies/:id': {
        get: {
            ctrl: 'MovieController',
            method: 'getMovie',
            public: true
        },
        post: {
            ctrl: 'MovieController',
            method: 'updateMovie',
            role: 'admin',
            public: true
        },
        delete: {
            ctrl: 'MovieController',
            method: 'deleteMovie',
            role: 'admin',
            public: true
        }
    },
    '/persons': {
        get: {
            ctrl: 'PersonController',
            method: 'getPersons',
            public: true
        },
        post: {
            ctrl: 'PersonController',
            method: 'createPerson',
            role: 'admin',
            public: true
        }
    },
    '/persons/:id': {
        get: {
            ctrl: 'PersonController',
            method: 'getPerson',
            public: true
        },
        post: {
            ctrl: 'PersonController',
            method: 'updatePerson',
            role: 'admin',
            public: true
        },
        delete: {
            ctrl: 'PersonController',
            method: 'deletePerson',
            role: 'admin'
        }
    },
    '/genres': {
        get: {
            ctrl: 'GenreController',
            method: 'getGenres',
            public: true
        },
        post: {
            ctrl: 'GenreController',
            method: 'createGenre',
            role: 'admin',
            public: true
        }
    },
    '/genres/:id': {
        get: {
            ctrl: 'GenreController',
            method: 'getGenre',
            public: true
        },
        post: {
            ctrl: 'GenreController',
            method: 'updateGenre',
            role: 'admin'
        },
        delete: {
            ctrl: 'GenreController',
            method: 'deleteGenre',
            role: 'admin'
        }
    }
}
