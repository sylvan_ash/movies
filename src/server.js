/**
 * This is the startup script for web server.
 */
'use strict'

if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'development'
}

var config = require('config')
var express = require('express')
var winston = require('winston')
var _ = require('underscore')
var morgan = require('morgan')
var serveStatic = require('serve-static')
var Path = require('path')
// var bodyParser = require('body-parser')
var expressBusboy = require('express-busboy')
var session = require('express-session')
var MongoStore = require('connect-mongo')(session)
var passport = require('passport')
var logger = require('./utilities/logger')
var createDomain = require('domain').create
var models = require('./models')
var Const = require('./utilities/const')
var UnauthorizedError = require('./utilities/errors').UnauthorizedError
var ForbiddenError = require('./utilities/errors').ForbiddenError

var app = express()
app.set('port', config.WEB_SERVER_PORT)
app.use(serveStatic(Path.join(__dirname, '../public')))

// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({extended: true}))
expressBusboy.extend(app, {
    upload: true,
    path: Path.join(__dirname, config.UPLOADS || '../public/uploads')
    // mimeTypeLimit: ['image/jpeg', 'image/png']
})

passport.serializeUser(function(user, done) {
    done(null, user.id)
})
passport.deserializeUser(function(userId, done) {
    models.User.findById(userId, done).populate('group')
})

app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: config.SESSION_SECRET,
    store: new MongoStore({url: config.MONGODB_URL})
}))
app.use(passport.initialize())
app.use(passport.session())

app.use(function(req, res, next) {
    var domain = createDomain()
    domain.add(req)
    domain.add(res)
    domain.run(function() {
        next()
    })
    domain.on('error', function(e) {
        next(e)
    })
})

// JSONP endpoints
app.get('/config', function(req, res) {
    res.jsonp(config.PUBLIC)
})
app.get('/me', function(req, res) {
    if (req.user) {
        res.jsonp(_.omit(req.user.toJSON(), 'password', 'salt'))
    } else {
        res.jsonp({})
    }
})

// Admin template
app.get('/admin', function(req, res, next) {
    console.log('Here!')
    res.sendFile('admin/index.html')
})

app.use(morgan('dev'))

// load routes
var routes = require('./routes.js')

var router = new express.Router()
_.each(routes, function(route, path) {
    _.each(route, function(endpoint, method) {
        var ctrl = require('./controllers/' + endpoint.ctrl)
        if (!ctrl[endpoint.method]) {
            throw new Error(`method ${endpoint.method} not found in controller ${endpoint.ctrl}`)
        }
        if (endpoint.role) {
            if (endpoint.role !== 'sa' && endpoint.role !== 'admin') {
                throw new Error(`Role must be empty or "sa" or "admin". Current: "${endpoint.role}"`)
            }
        }
        router[method](path, [
            function(req, res, next) {
                if (!endpoint.msg) {
                    endpoint.msg = ''
                }
                if (endpoint.public) {
                    return next()
                }
                if (!req.user) {
                    return next(new UnauthorizedError(endpoint.msg))
                }
                if (!endpoint.role || req.user.role === Const.roles.SUPER_ADMIN) {
                    return next()
                }
                if (endpoint.role === 'sa' || req.user.role === Const.roles.CLIENT) {
                    return next(new ForbiddenError(endpoint.msg))
                }
                next()
            },
            ctrl[endpoint.method]
        ])
    })
})
// mount API router
app.use('/api', router)

app.use(function(req, res) {
    res.status(404).json({error: 'route not found'})
})

app.use(function(err, req, res, next) {
    logger.logFullError(err, req.method + ' ' + req.url)
    res.status(err.httpStatus || 500).json({
        error: err.message
    })
})

app.listen(app.get('port'), function() {
    winston.info('Express server listening on port %d in %s mode', app.get('port'), process.env.NODE_ENV)
})
