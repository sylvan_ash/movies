"use strict"

/**
 * This file defines helper methods
 */

var co = require("co")
var _ = require("underscore")

/**
 * Escape special regex characters
 * @param {String} text the text to escape
 * @return {String} the escaped string
 */
RegExp.escape = function(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
}

/**
 * Create regexp that are not case sensitive
 * @param {String} text the text to match
 * @return {RegExp} the regexp
 */
function createCaseInsensitiveRegex(text) {
    return new RegExp("^" + RegExp.escape(text) + "$", "i")
}

/**
 * Wrap generator function to standard express function
 * @param {Function} fn the generator function
 * @return {Function} the wrapped function
 */
function wrapExpress(fn) {
    return function(req, res, next) {
        co(fn(req, res, next)).catch(next)
    }
}

/**
 * Round a number
 * @param {Number} number the number
 * @param {Number} digits the number of digits after decimal
 * @return {Number} the rounder number
 */
function roundNumber(number, digits) {
    if (number === null) {
        return number
    }
    return Number(parseFloat(number).toFixed(digits))
}

/**
 * Create search criteria from query string
 * @param {Object} query the query string
 * @param {Object} opts the options
 * @param {String[]} opts.allowedFilterValues the list of allowed properties
 * @param {String[]} opts.partialMatchFilter the list of partial matching properties
 * @param {String[]} opts.numberFilter the list of number properties
 * @param {String[]} opts.dateFilters the list of date properties
 * @return {Object} criteria
 */
function createCriteria(query, opts) {
    var criteria = {}
    _.each(opts.allowedFilterValues, prop => {
        if (query[prop]) {
            if (_.contains(opts.partialMatchFilter, prop)) {
                criteria[prop] = new RegExp(RegExp.escape(query[prop]), "i")
            } else if (_.contains(opts.numberFilter, prop)) {
                criteria[prop] = Number(query[prop])
            } else if (_.contains(opts.dateFilters, prop)) {
                var start = new Date(query[prop])
                var end = new Date(query[prop])
                start.setHours(0)
                start.setMinutes(0)
                start.setSeconds(0)
                end.setHours(23)
                end.setMinutes(59)
                end.setSeconds(59)
                criteria[prop] = {
                    $gte: start,
                    $lte: end
                }
            } else {
                criteria[prop] = query[prop]
            }
        }
    })
    return criteria
}

/**
 * Get a random number
 * @param {Number} min lowest value
 * @param {Number} max highest value
 * @return {Number} random number
 */
function getRandomInt(min = 10, max = 50) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min)) + min
}

/**
 * @param {String} query String to do a partial match with
 * @return {String} regular expression string
 */
function partialMatchFilter(query) {
    return new RegExp(RegExp.escape(query, 'i'))
}

module.exports = {
    createCaseInsensitiveRegex,
    wrapExpress,
    roundNumber,
    createCriteria,
    getRandomInt,
    partialMatchFilter
}
