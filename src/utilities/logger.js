/**
 * This module contains the winston logger configuration.
 */
"use strict"

var _ = require('underscore')
var winston = require('winston')
var util = require('util')
var config = require("config")
var Path = require("path")
var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({level: config.LOG_LEVEL}),
        new (winston.transports.File)({filename: Path.join(__dirname, '../../logs/app.log'), level: config.LOG_LEVEL})
    ]
})

/**
 * Log error details with signature
 * @param {Object} err the error
 * @param {Object} signature the signature
 */
logger.logFullError = function(err, signature) {
    if (!err) {
        return
    }
    var args = Array.prototype.slice.call(arguments)
    args.shift()
    winston.error.apply(winston, args)
    winston.error(util.inspect(err))
    winston.error(err.stack)
}

/**
 * Remove invalid properties from the object and hide long arrays
 * @param {Object} obj the object
 * @return {Object} the new object with removed properties
 * @private
 */
function _sanitizeObject(obj) {
    try {
        return JSON.parse(JSON.stringify(obj, function(name, value) {
            /* Array of field names that should not be logged */
            var removeFields = ["password", "salt"]
            if (_.contains(removeFields, name)) {
                return "<removed>"
            }
            if (_.isArray(value) && value.length > 30) {
                return "Array(" + value.length + ")"
            }
            return value
        }))
    } catch (e) {
        return obj
    }
}

/**
 * Wrap all functions of a service and log debug information if DEBUG is enabled
 * @param {Object} service the service to wrap
 */
logger.wrapFunctions = function(service) {
    if (config.LOG_LEVEL !== "debug") {
        return
    }
    _.each(service, function(method, name) {
        if (name === "createHash") {
            return
        }
        service[name] = function * () {
            logger.debug("ENTER " + name)
            logger.debug("input arguments")
            var args = Array.prototype.slice.call(arguments)
            if (name === "authenticate") {
                args[1] = "<removed>"
            }
            logger.debug(util.inspect(_sanitizeObject(args)))
            try {
                var result = yield method.apply(this, arguments)
                logger.debug("EXIT " + name)
                logger.debug("output arguments")
                logger.debug(util.inspect(_sanitizeObject(result)))
                return result
            } catch (e) {
                logger.logFullError(e, name)
                throw e
            }
        }
    })
}

module.exports = logger
