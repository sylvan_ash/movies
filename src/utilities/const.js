module.exports = {
    LimitTypes: {
        MINIMAL: 1, // The bare minimal (id + title/name)
        BASIC: 2, // id + title + slug + image
        HIGHLIGHTS: 3 // id + title + slug + highlight image + normal image
    }
}
