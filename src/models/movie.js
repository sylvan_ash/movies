var mongoose = require('mongoose')
var Schema = mongoose.Schema

var Actor = new Schema({
    role: String,
    // id: {type: ObjectId, ref: 'Person'}
    actor: {
        id: {type: String},
        name: {type: String}
    }
})

var Cast = new Schema({
    name: String,
    id: String
})

var Genre = new Schema({
    title: String,
    id: String
})

var movie = new Schema({
    title: {type: String, required: true},
    description: String,
    slug: {type: String, required: true, unique: true},
    releaseDate: Date,
    createdDate: {type: Date, default: Date.now()},
    mpRating: String, // Motion Picture Rating
    rating: Number, // Score of the movie out of 10
    actors: [Actor], // The roles in the movie plus the actors
    stars: [Cast], // The main stars of the movie
    directors: [Cast],
    genres: [Genre],
    highlight: {type: Boolean, default: false},
    image: {type: String, default: null},
    highlightImage: {type: String, default: null}
    // actors: [Actor],
    // stars: [{type: ObjectId, ref: 'Person'}],
    // directors: [{type: ObjectId, ref: 'Person'}]
})

module.exports = movie
