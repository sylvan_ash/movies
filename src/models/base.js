/**
 * This file contains a collection of base schema models for commonly used schemes
 */

var mongoose = require('mongoose')
var ObjectId = mongoose.Schema.Types.ObjectId

var Request = {
    user: {type: ObjectId, ref: 'User'},
    place: {type: ObjectId, ref: 'Place'},
    dateCreated: {type: Date, default: Date.now}
}

var FileUpload = {
    status: {type: String, required: true, default: 'pending'},
    s3Key: {type: String, required: true, default: 'pending'},
    url: {type: String, required: true, default: 'pending'},
    mediumS3Key: {type: String, required: false}
}

module.exports = {
    Request,
    FileUpload
}
