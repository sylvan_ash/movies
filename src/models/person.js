var mongoose = require('mongoose')
var Schema = mongoose.Schema
// var ObjectId = mongoose.Schema.Types.ObjectId

var person = new Schema({
    name: {type: String, required: true},
    dob: Date,
    bio: String,
    slug: {type: String, required: true, unique: true},
    image: String,
    // movies: [{type: ObjectId, ref: 'Movie'}],
    roles: [],
    dateCreated: {type: Date, default: Date.now}
})
person.index({name: 'text'})

module.exports = person
