var mongoose = require('mongoose')
var Schema = mongoose.Schema

var genre = new Schema({
    title: {type: String, required: true},
    dateCreated: {type: Date, default: Date.now}
})

module.exports = genre
