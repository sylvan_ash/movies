var mongoose = require('mongoose')
var Schema = mongoose.Schema

module.exports = new Schema({
    username: {type: String, required: true, unique: true},
    password: {type: String, required: false},
    salt: {type: String, required: false},
    firstname: {type: String, required: true},
    middlename: {type: String, required: false},
    lastname: {type: String, required: true},
    email: {type: String, required: true, unique: true}
})
