'use strict'

var _ = require('underscore')
var MovieService = require('../services/MovieService')
var wrapExpress = require('../utilities/utils').wrapExpress
var LimitTypes = require('../utilities/const').LimitTypes

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * createMovie(req, res) {
    var movie = yield MovieService.createMovie(req.body, req.files)
    res.json(movie)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getMovies(req, res) {
    var criteria = {}
    var limitType = null
    var sort = req.query.sort || 'title'
    if (req.query.highlight) {
        criteria.highlight = true
    }
    if (req.query.limited) {
        var values = _.values(LimitTypes)
        if (values.indexOf(Number(req.query.limitType)) !== -1) {
            limitType = Number(req.query.limitType)
        }
    }
    console.log(req.query)

    var movies = yield MovieService.getMovies(criteria, limitType, sort)
    res.json(movies)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getMovie(req, res) {
    var criteria = {}
    var movie = yield MovieService.getMovie(req.params.id, criteria)
    res.json(movie)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * updateMovie(req, res) {
    var movie = yield MovieService.updateMovie(req.params.id, req.body, req.files)
    res.json(movie)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * deleteMovie(req, res) {
    yield MovieService.deleteMovie(req.params.id)
    res.status(204).end()
}

module.exports = {
    createMovie: wrapExpress(createMovie),
    getMovies: wrapExpress(getMovies),
    getMovie: wrapExpress(getMovie),
    updateMovie: wrapExpress(updateMovie),
    deleteMovie: wrapExpress(deleteMovie)
}
