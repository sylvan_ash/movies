'use strict'

var UserService = require('../services/UserService')
var co = require('co')

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * createUser(req, res) {
    var user = yield UserService.createUser(req.body)
    res.json(user)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getUsers(req, res) {
    var users = yield UserService.getUsers()
    res.json(users)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getUser(req, res) {
    var user = yield UserService.getUser(req.params.id)
    res.json(user)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * updateUser(req, res) {
    var user = yield UserService.updateUser(req.params.id, {name: req.body.name})
    res.json(user)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * deleteUser(req, res) {
    yield UserService.deleteUser(req.params.id)
    res.status(204).end()
}

module.exports = {
    createUser: co.wrap(createUser),
    getUsers: co.wrap(getUsers),
    deleteUser: co.wrap(deleteUser),
    updateUser: co.wrap(updateUser),
    getUser: co.wrap(getUser)
}
