'use strict'

var PersonService = require('../services/PersonService')
var utils = require('../utilities/utils')

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * createPerson(req, res) {
    var person = yield PersonService.createPerson(req.body)
    res.json(person)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getPersons(req, res) {
    var criteria = {}
    if (req.query.q) {
        // criteria.name = utils.partialMatchFilter(req.query.q)
        criteria.name = {$regex: req.query.q, $options: 'i'}
    }
    var persons = yield PersonService.getPersons(criteria, req.query.limitType)
    res.json(persons)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getPerson(req, res) {
    var criteria = {}
    var person = yield PersonService.getPerson(req.params.id, criteria)
    res.json(person)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * updatePerson(req, res) {
    var person = yield PersonService.updatePerson(req.params.id, req.body)
    res.json(person)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * deletePerson(req, res) {
    yield PersonService.deletePerson(req.params.id)
    res.status(204).end()
}

module.exports = {
    createPerson: utils.wrapExpress(createPerson),
    getPersons: utils.wrapExpress(getPersons),
    getPerson: utils.wrapExpress(getPerson),
    updatePerson: utils.wrapExpress(updatePerson),
    deletePerson: utils.wrapExpress(deletePerson)
}
