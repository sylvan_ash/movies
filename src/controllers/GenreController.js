'use strict'

var GenreService = require('../services/GenreService')
var wrapExpress = require('../utilities/utils').wrapExpress

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * createGenre(req, res) {
    var genre = yield GenreService.createGenre(req.body)
    res.json(genre)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getGenres(req, res) {
    var genres = yield GenreService.getGenres()
    res.json(genres)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * getGenre(req, res) {
    var genre = yield GenreService.getGenre(req.params.id)
    res.json(genre)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * updateGenre(req, res) {
    var genre = yield GenreService.updateGenre(req.params.id, req.body)
    res.json(genre)
}

/**
 * @param {Object} req The Request object
 * @param {Object} res The Response object
 */
function * deleteGenre(req, res) {
    yield GenreService.deleteGenre(req.params.id)
    res.status(204).end()
}

module.exports = {
    createGenre: wrapExpress(createGenre),
    getGenres: wrapExpress(getGenres),
    getGenre: wrapExpress(getGenre),
    updateGenre: wrapExpress(updateGenre),
    deleteGenre: wrapExpress(deleteGenre)
}
