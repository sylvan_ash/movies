var gulp = require('gulp')
var gulpif = require('gulp-if')
var uglify = require('gulp-uglify')
var uglifycss = require('gulp-uglifycss')
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')
var env = process.env.GULP_ENV
var _ = require('underscore')
var adminRootPath = 'public/dist/admin'
var frontRootPath = 'public/dist/front'
var commonRootPath = 'public/dist/common'

var files = {
    shared: {
        js: [
            // App: Vendor JavaScripts
            'public/bower_components/jquery/dist/jquery.min.js',
            'public/bower_components/angular/angular.js',
            'public/bower_components/angular-ui-router/release/angular-ui-router.js',
            'public/bower_components/angular-resource/angular-resource.js',
            'node_modules/underscore/underscore.js'
        ],
        css: [
            'public/lib/bootstrap/bootstrap.css'
        ]
    },
    admin: {
        js: {
            vendor: [
                // Theme: Core + Morris Charts
                'public/lib/bootstrap/bootstrap.min.js',
                'public/bower_components/angularjs-datepicker/dist/angular-datepicker.js',
                'public/bower_components/angucomplete-alt/dist/angucomplete-alt.min.js',
                'public/bower_components/ng-file-upload/ng-file-upload.min.js'
                // 'public/bower_components/angular-file-upload/dist/angular-file-upload.min.js'
                // 'public/lib/morris/raphael.min.js',
                // 'public/lib/morris/morris.min.js',
                // 'public/lib/morris/morris-data.min.js'
            ],
            app: ['public/admin/app/**/*.js']
        },
        css: {
            vendor: [
                'public/lib/bootstrap/bootstrap.min.css',
                // 'public/lib/morris/morris.css',
                'public/lib/font-awesome/css/font-awesome.min.css',
                'public/bower_components/angularjs-datepicker/dist/angular-datepicker.css'
            ],
            app: ['public/admin/assets/css/**']
        },
        images: ['public/admin/assets/images/**'],
        fonts: [
            'public/lib/font-awesome/fonts/fontawesome-webfont.woff',
            'public/lib/font-awesome/fonts/fontawesome-webfont.ttf'
        ]
    },
    front: {
        js: {
            vendor: [],
            single: [
                'public/lib/flexisel/jquery.flexisel.js'
            ],
            app: ['public/front/app/**/*.js']
        },
        css: [
            // Vendor
            'public/lib/bootstrap/bootstrap.css',

            // App
            'public/front/assets/css/style.css'
        ],
        images: ['public/front/assets/images/**']
    }
}

/**
 * Task for building bundled JS file
 * @param {String} jsPath path to find files
 * @param {String|undefined} filename name of concatenated file
 * @param {String} destination location to place concatenated file
 * @return {*} gulp
 */
function buildJsTask(jsPath, filename, destination) {
    filename = filename || 'app.js'
    return gulp.src(jsPath)
        .pipe(concat(filename))
        .pipe(gulpif(env === 'prod', uglify))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(destination + '/js/'))
}

/**
 * Task for building stylesheets
 * @param {String} cssPath path to find files
 * @param {String|undefined} filename name of concatenated file
 * @param {String} destination location to place concatenated file
 * @return {*} gulp
 */
function buildCssTask(cssPath, filename, destination) {
    return gulp.src(cssPath)
        .pipe(concat(filename))
        .pipe(gulpif(env === 'prod', uglifycss))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(destination + '/css'))
        // .pipe(livereload)
}

/**
 * Task for moving images to dist folder
 * @param {String} imagesSrc path to find files
 * @param {String} destination location to place concatenated file
 * @return {*} gulp
 */
function moveImagesTask(imagesSrc, destination) {
    destination = destination || 'images'
    return gulp.src(imagesSrc)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(destination + '/images'))
}

/**
 * Task for moving images to dist folder
 * @param {String} fontsSrc path to find files
 * @param {String} destination location to place concatenated file
 * @return {*} gulp
 */
function moveFontsTask(fontsSrc, destination) {
    destination = destination || 'public/dist/fonts'
    return gulp.src(fontsSrc)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(destination + '/fonts'))
}

gulp.task('common:css', function() {
    buildCssTask(files.shared.css, 'common.css', commonRootPath)
})
gulp.task('common:js', function() {
    buildJsTask(files.shared.js, 'common.js', commonRootPath)
})

gulp.task('admin:js:vendor', function() {
    buildJsTask(files.admin.js.vendor, 'admin-vendor.js', adminRootPath)
})
gulp.task('admin:js:app', function() {
    buildJsTask(files.admin.js.app, 'admin-app.js', adminRootPath)
})
gulp.task('admin:css:vendor', function() {
    buildCssTask(files.admin.css.vendor, 'admin-vendor.css', adminRootPath)
})
gulp.task('admin:css:app', function() {
    buildCssTask(files.admin.css.app, 'admin-style.css', adminRootPath)
})
gulp.task('admin:images', function() {
    moveImagesTask(files.admin.images, adminRootPath)
})
gulp.task('admin:fonts', function() {
    moveFontsTask(files.admin.fonts, adminRootPath)
})

gulp.task('front:css', function() {
    buildCssTask(files.front.css, 'style.css', frontRootPath)
})
gulp.task('front:images', function() {
    moveImagesTask(files.front.images, frontRootPath)
})
gulp.task('front:vendor', function() {
    buildJsTask(files.front.js.vendor, 'vendor.js', frontRootPath)
})
gulp.task('front:app', function() {
    buildJsTask(files.front.js.app, 'app.js', frontRootPath)
})
gulp.task('front:single', function() {
    _.each(files.front.js.single, function(val) {
        gulp.src(val).pipe(gulp.dest(frontRootPath + '/js'))
    })
})

// gulp.task('admin-single', function () {
//     _.each(paths.admin.js.single, function (val) {
//         gulp.src(val).pipe(gulp.dest(adminRootPath + 'js/'));
//     });
//     _.each(paths.admin.css.single, function (val) {
//         gulp.src(val).pipe(gulp.dest(adminRootPath + 'css/'));
//     });
// });

gulp.task('build:common', ['common:css', 'common:js'])
gulp.task('build:front', ['front:css', 'front:images', 'front:vendor', 'front:single', 'front:app'])
gulp.task('build:admin', ['admin:js:vendor', 'admin:js:app', 'admin:css:vendor', 'admin:css:app', 'admin:images', 'admin:fonts'])
gulp.task('default', ['build:common', 'build:front', 'build:admin'])

// Watches
gulp.task('watch', function() {
    gulp.watch(files.front.js.app, ['build:front'])
    gulp.watch(files.admin.js.app, ['build:admin'])
})
